
using AdventureGame;
using System;
using System.IO;
using System.Collections.Generic;
using Functions;

namespace Game
{

    public class Base
    {

        private List<Character> chars;//Characters
        private List<Item> itms;//Items
        private List<Locations> loc;//Locations
        private List<string> msgs;//messages

        //========================= GET FUNCTIONS ==============================
        internal List<Character> Chars
        {
            get
            {
                return chars;
            }

            set
            {
                chars = value;
            }
        }

        internal List<Item> Itms
        {
            get
            {
                return itms;
            }

            set
            {
                itms = value;
            }
        }

        internal List<Locations> Loc
        {
            get
            {
                return loc;
            }

            set
            {
                loc = value;
            }
        }

        internal List<string> Msgs
        {
          get
          {
            return msgs;
          }

          set
          {
            msgs = value;
          }
        }

            public Base() { }



	};


  //+++++++++++++++++++++++++++++++ G  L O B A L    F U N C T I O N S++++++++++++++++++++++++++
    public static class Global
    {
        static Random rand = new Random();
        public static int a = 0;  // CURRENT LOCATION
        public static int invSize = 10; // Characters inventory size

        // Input Objects From File
        public static void inputObjects(List<Item> it)
        {
          string[] lines = System.IO.File.ReadAllLines("Data/objects.dat");
          foreach (var line in lines)
          {
            var lineSplit = line.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var stuff = new Item (lineSplit [0], lineSplit [1], Int32.Parse (lineSplit [2]), Int32.Parse (lineSplit [3]), Int32.Parse (lineSplit [4]), Int32.Parse (lineSplit [5]), Int32.Parse (lineSplit [6]));
            it.Add(stuff);
          }

        }
        // Input Locations From File
        public static void inputLoc(List<Locations>loc_list)
        {
          string[]  lines = System.IO.File.ReadAllLines("Data/Locations.dat");
          foreach (var line in lines)
          {
            var lineSplit = line.Split (new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Locations _loc = new Locations (lineSplit [0], lineSplit [1], Int32.Parse (lineSplit [2]), bool.Parse(lineSplit [3]) );
            loc_list.Add(_loc);

          }
          return;
        }

        // Input Location's Functions From File
        public static void inputLocFuncs(Base dat)
        {
          string[]  lines = System.IO.File.ReadAllLines("Data/LocationFuncs.dat");
          for(int i=0;i<lines.Length;i++)
          {
            var func = lines[i].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            dat.Loc[i].func=new List<string> (func);
          }
          return;
        }

        //=-=-=-=-=--=-=-=-=-= M E N U -=-=-=-=-=-=-=-=-=-=
        public static void MENU(Base Data)
        {
            //Load(*Data);

            Character Char=new Character();//you

            //checking Race
            foreach(var player in Data.Chars)
            {
              if (player.get_name != "Human" || player.get_name != "ELf" || player.get_name != "Orc") {
                Char = player;
              } else
                return;
            }


            string invController ;   // Invertory case controller


            while (true)
            {

                Console.Clear();
                if (Data.Loc[a].firstTime)
                {   // Do everything for the first time here
                    Console.Clear();
                    Console.WriteLine("Welcome, {0} to {1}!",Char.get_name,Data.Loc[a].loc_name);
                    Data.Loc[a].firstTime = false;
                    // Get the welcome messages ready for the first time and close the file

                }
                else
                {                   // Not first time already just show message from a static List
                    Console.Clear();

                    if (Data.Msgs.Count != 0)
                    {
                        //Console.Clear();
                        Console.WriteLine(Data.Msgs[rand.Next ( Data.Msgs.Count ) ]);   // display :>
                    }
                    else
                    {
                        Console.WriteLine("Welcome back, " + Char.get_name + "!");
                    }

                    Console.WriteLine();
                    Console.WriteLine();
                }



                Console.Write("\nLocations:" + String.Format("{0,-20}", Data.Loc[Global.a].loc_name) + "\n");
                Console.Write("              <<<</  \\>>>>\n");
                Console.Write("[<<<<<<<<<<<<<<<<<{||}>>>>>>>>>>>>>>>>>]\n");
                Console.Write("|******-------------------------*******|\n");
                Console.Write("|****-----Choose what U want------*****|\n");
                Console.Write("|***------1." + String.Format("{0,-20}", Data.Loc[Global.a].func[0]).Replace(" ","-") + "---****|\n");
                Console.Write("|**-------2.Travel------------------***|\n");
                Console.Write("|**-------3.Profile-info------------***|\n");
                Console.Write("|***------4." + String.Format("{0,-20}", Data.Loc[Global.a].func[1]).Replace(" ","-") + "---****|\n");
                Console.Write("|****-----5." + String.Format("{0,-20}", Data.Loc[Global.a].func[2]).Replace(" ","-") + "--*****|\n");
                Console.Write("|******-------------------------*******|\n");
                Console.Write("[<<<<<<<<<<<<<<<<<{||}>>>>>>>>>>>>>>>>>]\n");
                Console.Write("              <<<<\\  />>>> \n");


                string c;//choose MENU function  /\====
                string in_;//choose inventory function
                c = Console.ReadLine();
                switch (c)
                {
                    //FUNCTIN_1 (QUESTS)
				case "1":
					if (a == 0) {
						Function.MAX_HEALTH = Char.get_health;
						Function.arena (Char, Data.Chars);
					}
                        else if (a == 2)
                            //Wizard
                              Function.arena(Char, Data.Chars);
                        else
                            //other quests
                              Function.arena(Char, Data.Chars);
                        break;

                    //TRAVEL
                    case "2":
                        Data.Loc[Global.a].travel(Char, Data);
                        break;

                    //INVENTORY
                    case "3":
                        while (true)
                        {
                            Console.Clear();
                            Char.update(Char.info);//update character's information
                            for (int i = 0; i < 9; i++)
                            {
                                if (i == 0 || i == 8)//printing table
                                    Console.WriteLine(Char.info[i] + "+");
                                else Console.WriteLine(Char.info[i] + "|");
                            }
                            Console.WriteLine();

                            Console.WriteLine("1. Inventory \n");
                            Console.WriteLine("2. Map \n");
                            Console.WriteLine("3. Quests\n");
                            Console.WriteLine("4. Exit \n");
                            in_ = Console.ReadLine();
                            switch (in_)
                            {   // 1. Inventory
                                case "1":
                                    Function.inv_table(Char);

                                        break;
                                // 2. Map
                                case "2":
                                    Function.map(Data.Loc);

                                    Console.WriteLine("\nType \"Quit\" to go back!");
                                        do
                                        { // get tolower and check
                                            invController = Console.ReadLine();
                                            invController = invController.ToLower();
                                        } while (invController != "quit");
                                        break;
                                // 3. Quests
                                case "3":
                                      // NO QUESTS YET
                                        break;

                                // 4. Exit
								case "4":
										break;

                                    default:
                                        break;
                             }
						if (in_ == "4")
							break;
                     }//while inv
                    break;

                    //FUNCTION_4 (SHOPS)
                    case "4":

                        //if(shoploc=="Nemertea" || shoploc=="Wizard's Tower" || shoploc=="Flaming Stones")
                        if(a == 0 || a == 1 || a == 2)
                          Function.shop (Char, Data, Data.Loc [a].shop_tbl);
                        //if(shoploc==)  functions for other towns
                        else
                            Console.WriteLine("DAYUM");
                        break;

                    //Exit or something
                    case "5":
                        if (a == 0 || a == 1 || a == 3)
                            return;
                        break;

                    default:
                      break;

                }//switch

            }//while

        }//menu



    }//GLOBAL

}//Game
