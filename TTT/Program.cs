﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using AdventureGame;
using Game;
using Functions;

namespace Adventure
{
    class Program
    {
        static void Main(string[] args)
        {
            // WINDOWS STUFF
            Console.SetWindowSize (115, 50);
            Console.SetBufferSize (135, 60);
            Console.SetWindowPosition (0, 4);


            string choice;// choose race
            string name;// choose name

            // CREATE BASE
            Base Data = new Base();


            //=========CREATE CHARACTERS================
            List<Character> plr = new List<Character>();

            Human A = new Human();
            plr.Add(A);
            Orc B = new Orc();
            plr.Add(B);
            Elf C = new Elf();
            plr.Add(C);

            Data.Chars = plr;


            //=---=-==-==CREATE ITEMS=-=-=-=-=-=-==-
            List<Item> it = new List<Item>();
            List<Locations> loc = new List<Locations> ();
            Global.inputObjects(it);    // input objects from object.dat file
            Data.Itms=it;



            //-=--=-=-= CREATE LOCATIONS=-=-=-=-=-=-=-=
            Global.inputLoc(loc);
            Data.Loc = loc;
            Global.inputLocFuncs(Data);



            //**********WELCOME MESSEAGES***********
            List<string> welcomeMsgs=new List<string>(); // Make it static damn it
            string tempString;
            StreamReader wlcMsgs=new StreamReader("Data/wel-msgs.dat");       // declare

            while (!wlcMsgs.EndOfStream)
            {    // get
                tempString=wlcMsgs.ReadLine();
                welcomeMsgs.Add(tempString);  // add
            }
            wlcMsgs.Close();
            Data.Msgs = welcomeMsgs;
            //**************************************/



            //new game ,load,save
            Console.Clear();


            //_+_+_+_+_+_+_+_+_+_+_+_ I N T R O D U C T I O N _+_+_+_+_+_+_+_+_+_
            Console.WriteLine( "\n********************************************************\n" +
                                 "* Hello!! My name is Ted and this is Adventure game!!! *\n" +
                                 "********************************************************\n");
            Console.WriteLine( "1.New Game\n");
            Console.WriteLine( "2.Load Game\n");
            Console.WriteLine( "3.Exit \n");
            string nlx = Console.ReadLine();

            Human h1 = new Human();
            Orc o1 = new Orc();
            Elf e1 = new Elf();


            switch (nlx)
            {
                case "1":
                    //Console.Clear();
                    Console.Write( "Choose Your Character (Human,Orc,Elf): ");
                    choice = Console.ReadLine();
                    Console.Write( "Insert Name (MAX.10 symb): ");
                    name = Console.ReadLine();
                    //-------------------------------------------------------------------

                    //&&&&&&&&& MAKE STRING TOLOWER &&&&&&&&&&&
                    choice = choice.ToLower();
                    //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&




                    //**************DEFAULT INVENTORY***********
                    if (choice == "human")
                    {
                        h1.set_name=name;
                        h1.set_player=true;
                        Data.Chars.Add(h1);
					Function.hum_def_items(Data.Chars.Last(), Data.Itms);

                    }
                    else if (choice == "orc")
                    {

                        o1.set_name = name;
                        o1.set_player = true;
                        Data.Chars.Add(o1);
					Function.orc_def_items(Data.Chars.Last(),  Data.Itms);

                    }
                    else if (choice == "elf")
                    {
                        e1.set_name = name;
                        e1.set_player = true;
                        Data.Chars.Add(e1);
					Function.elf_def_items(Data.Chars.Last(),  Data.Itms);
                    }
                    else
                    {
                        Console.WriteLine( "something gone wrong! \n");
                        return;
                    }

                    Global.MENU(Data);
                    break;
                case "2":
                    Function.Load(Data);
                    Global. MENU(Data);
                    break;
                default:
                    break;

            }
			
			Function.Save(Data,Data.Chars.Last());//loadidan ver aketebs

            return;

        }
    }
}
