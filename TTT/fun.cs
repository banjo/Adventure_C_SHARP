﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Game;
using AdventureGame;
namespace Functions
{
  public static class Function
  {
    public static Random rand = new Random();

    // Set Items's properties
    public static void prop(Character a,Item x)
    {
      a.set_attack=a.get_attack+x.attack;
      a.set_defence=a.get_defence+x.defence;
      a.set_stamina=a.get_stamina+x.agility;
    }
	// delete Item's properties
	public static void del_prop(Character a,Item y)//delete Items's properties
	{

		a.set_attack=a.get_attack-y.attack;
		a.set_defence=a.get_defence-y.defence;
		a.set_stamina=a.get_stamina-y.agility;

	}

    // Set human's default Gear
    public static void hum_def_items(Character Char,List<Item> itms)
    {
      Char.Gear[0]=itms[1];
      Char.Gear[1]=itms[4];
      Char.Gear[2]=itms[7];
      prop(Char,itms[1]);
      prop(Char,itms[4]);
      prop(Char,itms[7]);
    }

    // Set orc's default Gear
    public static void orc_def_items(Character Char,List<Item> itms)
    {
      Char.Gear[0]=itms[7];
      prop(Char,itms[7]);
    }

    // Set elf's default Gear
    public static void elf_def_items(Character Char,List<Item> itms)
    {
      Char.Gear[0]=itms[2];
      Char.Gear[1]=itms[6];
      prop(Char,itms[2]);
      prop(Char,itms[6]);
    }

    // Print character's information for battle or sam sungs
    public static void printChars(List<Character> ch)
    {
      for (int x = 0;x < ch.Count;x++) {
        ch[x].update(ch[x].info);
      }
      for (int i = 0; i < 9; i++) {
        for (int j = 0; j < ch.Count; j++) {
          if(i==0 || i==8)
            Console.Write(ch[j].info[i]+"+");
          else Console.Write(ch[j].info[i]+"|");
        }
        Console.WriteLine ();
      }
      return;
    }

    // Create Inventory table
    public static void inv_table(Character c)
		{
			while (true) {
			// ok nevermind
			Console.Clear ();

			Console.Write ("  ____________________________________________________________________________  \n");
			Console.Write (" /.........................................................................../| \n");
			Console.Write ("/.........................................................................../.| \n");
			Console.Write ("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$..| \n");
			Console.Write ("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$..| \n");
			Console.Write ("$$$$ Gold:" + String.Format ("{0,-8}", c.get_gold) + "$$$$$$$$$$$   W E A P O N S   $$$$$$$$$$$$$$$$$$$$$$$$$$$$..| \n");
			Console.Write ("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$..| \n");
			Console.Write ("$$$$$$$$$$$$$$$$$$ ==== 1 ===== $$$$$$$$$$$$ ==== 2 ===== $$$$$$$$$$$$$$$$$$..| \n");
				Console.Write (String.Format ("$$$$$$$$$$$$$$$$$$|  {0,-10}|$$$$$$$$$$$$|  {1,-10}|$$$$$$$$$$$$$$$$$$..| \n", c.Gear [0].name, c.Gear [1].name ));
				Console.Write (String.Format ("$$$$$$$$$$$$$$$$$$|   {0,-9}|$$$$$$$$$$$$|   {1,-9}|$$$$$$$$$$$$$$$$$$..| \n", c.Gear [0].type, c.Gear [1].type ));
				Console.Write (String.Format ("$$$$$$$$$$$$$$$$$$| Attack: {0,-3}|$$$$$$$$$$$$| Attack: {1,-3}|$$$$$$$$$$$$$$$$$$..| \n", c.Gear [0].attack, c.Gear [1].attack ));
				Console.Write (String.Format ("$$$$$$$$$$$$$$$$$$| Defenc: {0,-3}|$$$$$$$$$$$$| Defenc: {1,-3}|$$$$$$$$$$$$$$$$$$..| \n", c.Gear [0].defence, c.Gear [1].defence ));
				Console.Write (String.Format ("$$$$$$$$$$$$$$$$$$| Agilit: {0,-3}|$$$$$$$$$$$$| Agilit: {1,-3}|$$$$$$$$$$$$$$$$$$..| \n", c.Gear [0].agility, c.Gear [1].agility ));
			Console.Write ("$$$$$$$$$$$$$$$$$$ ============ $$$$$$$$$$$$ ============ $$$$$$$$$$$$$$$$$$..|  ___   ___   ___    ___   \n");
			Console.Write ("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$..| |     |     |   |  |   |  \n");
			Console.Write ("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$     A R M O R     $$$$$$$$$$$$$$$$$$$$$$$$$$$$..| |  _  |--   |---|  |---   \n");
			Console.Write ("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$..| |___| |___  |   |  |   \\ \n");
			Console.Write ("$$$$ ==== 3 ===== $$$$ ==== 4 ===== $$$$ ==== 5 ===== $$$$ ==== 6 ===== $$$$..| \n");
			Console.Write (String.Format ("$$$$|  {0,-10}|$$$$|  {1,-10}|$$$$|  {2,-10}|$$$$|  {3,-10}|$$$$..| \n", c.Gear [2].name, c.Gear [3].name, c.Gear [4].name, c.Gear [5].name));
			Console.Write (String.Format ("$$$$|   {0,-9}|$$$$|   {1,-9}|$$$$|   {2,-9}|$$$$|   {3,-9}|$$$$..| \n", c.Gear [2].type, c.Gear [3].type, c.Gear [4].type, c.Gear [5].type));
			Console.Write (String.Format ("$$$$| Attack: {0,-3}|$$$$| Attack: {1,-3}|$$$$| Attack: {2,-3}|$$$$| Attack: {3,-3}|$$$$..| \n", c.Gear [2].attack, c.Gear [3].attack, c.Gear [4].attack, c.Gear [5].attack));
			Console.Write (String.Format ("$$$$| Defenc: {0,-3}|$$$$| Defenc: {1,-3}|$$$$| Defenc: {2,-3}|$$$$| Defenc: {3,-3}|$$$$..| \n", c.Gear [2].defence, c.Gear [3].defence, c.Gear [4].defence, c.Gear [5].defence));
			Console.Write (String.Format ("$$$$| Agilit: {0,-3}|$$$$| Agilit: {1,-3}|$$$$| Agilit: {2,-3}|$$$$| Agilit: {3,-3}|$$$$..| \n", c.Gear [2].agility, c.Gear [3].agility, c.Gear [4].agility, c.Gear [5].agility));
			Console.Write ("$$$$ ============ $$$$ ============ $$$$ ============ $$$$ ============ $$$$..| \n");
			Console.Write ("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$..| \n");
			Console.Write ("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$./  \n");
			Console.Write ("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$/   \n\n");

			var col = 0;
			Console.WriteLine (" ==========================================================================");
			for (int i = 0; i < Game.Global.invSize; i++) 
			{
				if (c.Inv [i].name == "empty")
					Console.Write ("|               empty                |");
				else
					Console.Write ("| {0,-6} {1,-6} At:{2,-3} Df:{3,-3} Ag:{3,-3} |", c.Inv [i].name, c.Inv [i].type.ToString (), c.Inv [i].attack, c.Inv [i].defence, c.Inv [i].agility);

				col++;
				if (col % 2 == 0 || i == Game.Global.invSize - 1)
						Console.WriteLine ("({0}-{1})",i,i+1);
			}

		Console.WriteLine (" ==========================================================================");
		

		string inv_f;
		Console.WriteLine ("JUST DO IT!!!!");
		Console.WriteLine ("1. Move to Inventory ");
		Console.WriteLine ("2. Move to Gear ");
		Console.WriteLine ("3. Sell Item ");
		Console.WriteLine ("4. Exit ");


				inv_f=Console.ReadLine ();

				switch (inv_f) {

				case "1":
				
					Console.Write ("\nChoose the Slot: ");
					int MTI = Int32.Parse (Console.ReadLine ());
					c.MoveToInv (c,(GearType)(MTI - 1));
					break;
				case "2":
					Console.Write ("\nChoose the Index of Item: ");
					int MTG = Int32.Parse (Console.ReadLine ());
					c.MoveToGear (c,MTG-1);
					break;
				case "3":
					Console.Write ("\nChoose the Index of Item: ");
					int itm = Int32.Parse (Console.ReadLine ()) - 1;
					c.set_gold = c.get_gold + c.Inv [itm].Sgold;
					Console.Write ("You earned {0} Gold.", c.Inv [itm].Sgold.ToString ());
					c.delete (itm);
					Console.Read ();
					break;
				default:
					break;
				}
				if (inv_f == "4")
					break;
			}

		}



    // Create Map
    public static void map(List<Locations> dat)
    {

      string []l =new string[10];

      for (int i = 0; i < l.Length; i++)
        l [i] = "   ";

      l[Global.a] = "You";
      char []X=new char[dat.Count];

      for (int i = 0; i < dat.Count; i++) {
        if(dat[i].firstTime)//is true
          X[i]='X';
        else
          X[i]='.';
      }

      /********************
        0. Nemertea = home town
        1. Flaming Stones
        2. Wizard Tower
        3. Shadow Mountain
        4. Golden Forest
        5. Dead Lake
        6. Orc's Stronghold
        7. Hjortar Hus
        8. Black Fall
        9. Final BossAssNigga
        *********************/
			Console.WriteLine ();

			for(int i = 0; i < 101; ++i)
			{
				Console.Write("\r Checking {0}%   ", i);
				System.Threading.Thread.Sleep (5);
			}
			Console.WriteLine ();

      Console.Write(".====================================================================================================.\n"+
        "|                          ~~~~~~           .. -N-                                           .       |\n"+
        "|......                   | "+l[8]+"  |       .-     |                .                  `......-         |\n"+
        "|     :`                  | B F  |       :                  .....                  .-                |\n"+
        "|     ..   ~~~~~~          ~~~~~~       -.                 -.                    ~~~~~~              |\n"+
        "|     `:--| "+l[6]+"  | "+X[6]+".-.        :       :`                ~~~~~~                 | "+l[4]+"  |             |\n"+
        "|      :` | O S  |    `-..```.."+X[8]+"....../         ....."+X[1]+".-| "+l[1]+"  |......   .      | G F  |             |\n"+
        "|     `:   ~~~~~~         ```         :.    -...       `| F S  |              ./ ~~~~~~              |\n"+
        "|    `:                                .- --             ~~~~~~             --"+X[4]+"                      |\n"+
        "|    -`          ..                     :                  :               /                         |\n"+
        "|    :`        :.  -.                   :                  :`             :                          |\n"+
        "|    /         :    :                ~~~~~~~            `-..              :                 `        |\n"+
        "|    `"+X[7]+"`        :   `.-`            |  "+l[0]+"  |           :`       `.      `-              `.-`        |\n"+
        "|    ~~~~~~~~   :      :`       ....|  NE   |..-.        `.../-..`       /..-`         .-.           |\n"+
        "|   |  "+l[7]+"   | -.      `:      :`    ~~~~~~~    -.            ...........`   `:.    .--`             |\n"+
        "|   |        |"+X[7]+"-        :-`  `:`        :        `+......                      `-.:-`                | \n" +
        "| | |  H  H  |           .:--.          :        :`      --.        --:            .....`          | |\n"+
        "| W- ~~~~~~~~           -.               :       :          .......-  -.                ......... -E |\n"+
        "| |                  .--             `-..`       `....`                :.-.      `..               | |\n"+
        "|              /-----                :`               .-.              :  `......`  ..               |\n"+
        "|             .`               .-....`--                :              .-                          ..|\n"+
        "|           /`                 /        ........        `               /                         `- |\n"+
        "|           |              `...`                -.                      `:              .......`..-  |\n"+
        "|           /             `:                     .`                      -..```   ...-.        `     |\n"+
        "|  ~~~~~~ -"+X[5]+".-.         ..."+X[3]+"                     `|"+X[9]+"----.                    ``.:/:                  |\n"+
        "| | "+l[5]+"  |`   ':........    |               ......'    ~~~~~~~~~~                 : ..``             |\n"+
        "| | D L  |       .:         ~~~~~         -.          |   "+l[9]+"    |                :  ~~~~~~~         | \n"+
        "|  ~~~~~~          :       | "+l[3]+" |"+X[3]+"......:            |  FINAL   |                : |  "+l[2]+"  |        | \n"+
        "|                  :       | S M |                    |   BOSS   |                "+X[2]+".|  W T  |        |\n"+
        "|                  :        ~~~~~               |     |          |                   ~~~~~~~         |\n"+
        "|                  :                           -S-     ~~~~~~~~~~                                    |\n"+
        "|=============================================Legend=================================================|\n"+
        "|  NE +> Nemertea    |   OS +> " +((X[6] == '.') ? "Orc's Stronghold" : "                ")+  "   |  GF  +> "+((X[4] == '.') ? "Golden Forest" : "             ")+"    |  WT +> "+((X[2] == '.') ? "Wizard's Tower" : "              ")+" |\n"+
        "|  HH +> "  +((X[7] == '.') ? "Hjortar Hus" : "          ")+  "  |   SM +> " + ((X[3] == '.') ? "Shadow Of Mountain" : "                  ") + " |  FS  +> " + ((X[1] == '.') ? "Flaming Stones" : "              ")+  "   |  DL +> " + ((X[5] == '.') ? "Dead Lake" : "         ") + "      |\n"+
        "|  BF +> " + ((X[8] == '.') ? "Black Fall" : "          ") + "  |   X  +> BARIER             |  YOU +> Current location |                       |\n"+
        "'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'");

    }


    //++++++++++++++++++++++ B A T T L E +++++++++++++++++++++++++++++


    // if someone's dead ...prints winner
    public static void dead(Character char1,Character char2)
    {


      char1.addxp(15);//give xp
      char2.addxp(10);
      char1.set_gold=char1.get_gold+rand.Next(30,100);//give gold
      char2.set_health=0;
      Console.Write("Enemy recoveried!\n");

      Console.WriteLine ();
      Console.Write("\t   > WINNER < \n");
      Console.Write("\t.############<-\n");
      Console.Write("\t .#  "+String.Format("{0,-10}",char1.get_name)+"#<-\n");
      Console.Write("\t.############<-\n\n");
      Console.Write("Press enter to continue...");

      Console.Read ();
      return;

    }


    //if attacked and how much
    public static bool attack(Character p1,Character p2)
    {
      if((p1.get_speed+p1.get_stamina) >= (p2.get_speed+p2.get_stamina))//if you are faster
      {
        int hp=0;
        hp=p1.hit();//hit power

        int d=p2.get_defence-hp;
        int h=p2.get_health-hp;

        if (p2.get_defence > 1)
          p2.set_defence = d;
        else
          p2.set_health=h ;

        if(p2.get_defence < 1)
          p2.set_defence=0;

        if(p2.get_health<1)//if enemy is dead
        {
          dead(p1,p2);
          return true;
        }

        p2.inc_speed();//increase speed
        Console.Write("{0}'s health: {1}",p2.get_name,(p2.get_health+p2.get_defence).ToString()+" | \n");
        p1.sub_speed();//decrease speed
        return false;

      }
      else
        return attack(p2,p1);

    }

    //same defence
    public static bool defence(Character p1,Character p2)
    {
      int hp=p2.hit()/2;
      int d=p1.get_defence-hp;
      int h=p1.get_health-hp;

      if (p1.get_defence > 1)
        p2.set_defence = d;
      else
        p1.set_health=h ;

      if(p1.get_defence < 1)
        p1.set_defence=0;

      if(p1.get_health<1){
        dead(p2,p1);
        return true;
      }
      p1.inc_speed();//increase speed
      p1.inc_speed();//increase speed
      Console.Write("{0}'s health: {1}",p2.get_name,(p2.get_health+p2.get_defence).ToString()+" | \n");
      Console.Write("Blocked "+hp.ToString()+"\n\n");
      p2.sub_speed();
      return false;

    }

    //start battle...
    public static bool battle(Character char1,Character char2)
    {
       int  reset1=char2.get_health;
       int reset2=char2.get_speed;
       int reset3=char1.get_speed;

      while(true)
      {
        //Console.Write(reset1+" "+reset2+" "+reset3;


        Console.WriteLine(char1.get_name+ " VS " +char2.get_name);
        Console.Write("Make your Command!!! \n");
        Console.Write(" 1. ATTACK \n");
        Console.Write(" 2. FULL ATTACK\n");
        Console.Write(" 3. DEFENCE \n");
        Console.Write(" 4. SURRENDER \n");

        string comm=Console.ReadLine();
        Console.Clear();
        switch (comm) {
        case "1":
        case "2":
          while(true)
          {

            if(attack(char1,char2)){
              char2.set_health=reset1;
              char2.set_speed=reset2;
              return true;
            }

            if(comm == "1")
              break;
          }
          break;
        case "3": // funqciebshia gasaketebeli..jer mezareba :D
          if(defence(char1,char2)){
            char1.set_speed=reset3;
			return true;
          }
          break;
		case "4":
			Console.Write ("RUN YOU FOOL !!!!\n");
			Console.Read ();
			return false;
        default:
          break;
        }
        //break;
      }

    }

    // battle map... random enemy
    public static Character morph(List<Character> plrs)
    {


		int N=15;
		int M=25;

		char [,]v = new char[25,35];
		int a=5;
		int b=5;

		int c=8;
		int d=10;

		int e=2;
		int f=17;

		int g=3;
		int h=2;

      Character X=plrs[0];
      Character Y=plrs[1];
      Character Z=plrs[2];
      Character enemy;

      //return enemy = X;//morph disabled
	

				
      for (int i=0; i!=N; ++i)
        for (int j=0; j!=M; ++j){
          if(i==0 || i==N-1)
            v[i,j] = '=';
          else if(j==0 || j==M-1){
            v[i,0]='|';
            v[i,M-1]='|';
          }
          else
            v[i,j]=' ';
        }
	
	  



      while(true){
		
        v[a,b]= 'Y';
        v[c,d]= 'H';//human
        v[e,f]= 'O';//orc
        v[g,h]= 'E';//elf

		Console.SetCursorPosition (0, 10);
        for (int i=0; i!=N; i++){
          for (int j=0; j!=M; j++)
            Console.Write(v[i,j]);
          Console.WriteLine ();
        }

		

        v[a,b]= ' ';
        v[c,d]= ' ';
        v[e,f]= ' ';
        v[g,h]= ' ';


		

		a-=(a>N-4)? 1:(rand.Next(-1,2));
		a+=(a<2)? 1: (rand.Next(-1,2));

		b-=(b>M-4)? 1:(rand.Next(-1,2));
		b+=(b<2)? 1: (rand.Next(-1,2));

		c-=(c>N-4)? 1:(rand.Next(-1,2));
		c+=(c<2)? 1: (rand.Next(-1,2));

		d-=(d>M-4)? 1:(rand.Next(-1,2));
		d+=(d<2)? 1: (rand.Next(-1,2));

		e-=(e>N-4)? 1:(rand.Next(-1,2));
		e+=(e<2)? 1: (rand.Next(-1,2));

		f-=(f>M-4)? 1:(rand.Next(-1,2));
		f+=(f<2)? 1: (rand.Next(-1,2));

		g-=(g>N-4)? 1:(rand.Next(-1,2));
		g+=(g<2)? 1: (rand.Next(-1,2));

		h-=(h>M-4)? 1:(rand.Next(-1,2));
		h+=(h<2)? 1 :(rand.Next (-1, 2));

			



        if(a==c && b==d){
          enemy = X;
          break;
        }

        if(a==e && b==f){
          enemy = Y;
          break;
        }
        if(a==g && b==h){
          enemy = Z;
          break;
        }

      } // End of While

				return enemy;




    }

		public static int MAX_HEALTH=100;
    // Arena
    public static void arena(Character Char,List<Character> plrs)
    {
			
			while (Char.get_health > 1) {
				Console.Clear ();
				
				printChars (plrs);// print characters properties

			
				Console.Write ("Press any key to continue...Type q to EXIT ARENA \n");
				string exit = Console.In.ReadLine ();
				if (exit == "q")
					return;



				if (!battle (Char, morph (plrs)))
					return;
			}

			Console.Write ("You have to recover until you battle again!\n");
			System.Threading.Thread.Sleep (2000);
			for (int i = 0; i < MAX_HEALTH; i++) {
				Console.Write ("\r {0} health = {1}", Char.get_name, Char.get_health);
				Char.set_health = i;
				System.Threading.Thread.Sleep (1000);
			}
			return;

		

      
    }





    //____________________________S  H  O  P __________________________________


    //shop table blocks (shopping)
    public static void shcase(Character x,Item i,Base Data)//,string n,string t)
    {
      if(i.type.ToString()=="Armor")
      {

        x.insertArmor(x, i, Data.Itms);
        return;
      }


      if(x.get_gold>=i.Bgold)
      {

        if (x.insertItem (i))
        {
          Console.WriteLine ("You bought {0} {1}  !!!", i.name, i.type.ToString ());
          Function.prop (x, i);
          x.set_gold=x.get_gold-i.Bgold;

        }

      }
      else {Console.Write("You dont have enough gold!\n");}
    }

    //check shop files
    public static bool shop_check(List<Item> itms,List<Item> shop_v, string loc)
    {

      

      //SteamReader=loc.c_str());
      string[] lines = System.IO.File.ReadAllLines(loc);
      foreach (var line in lines)
      {
        var lineSplit = line.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

        
      //X.type = Int32.Parse (lineSplit [1]);
        //if (itms.Contains (X))
        foreach(Item it in itms)
        {
			if (it.name == lineSplit[0] && it.type.ToString() == lineSplit[1]) {

            shop_v.Add (it);
            break;
          }
        }
      }
      if(shop_v.Count!=8)
        return false;

      return true;
    }

    //Create shop table
    public static void shop_table(List<Item> v)
    {

      Console.Clear ();

        Console.WriteLine(" __________________________________________________________________________________");
        Console.WriteLine("|\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ ");
        Console.WriteLine("|\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ ");
        Console.WriteLine("|\\\\\\₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪ ");
        Console.WriteLine("|\\\\\\₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪ ");
        Console.WriteLine("|\\\\\\₪₪₪₪₪ =====1====== ₪₪₪₪₪ =====2====== ₪₪₪₪₪ =====3====== ₪₪₪₪₪ =====4====== ₪₪₪₪₪ ");
      Console.WriteLine( String.Format("|\\\\\\₪₪₪₪₪|  {0,-10}|₪₪₪₪₪|  {1,-10}|₪₪₪₪₪|  {2,-10}|₪₪₪₪₪|  {3,-10}|₪₪₪₪₪",v[0].name,v[1].name,v[2].name,v[3].name));
      Console.WriteLine( String.Format("|\\\\\\₪₪₪₪₪|   {0,-9}|₪₪₪₪₪|   {1,-9}|₪₪₪₪₪|   {2,-9}|₪₪₪₪₪|   {3,-9}|₪₪₪₪₪",v[0].type.ToString(),v[1].type.ToString(),v[2].type.ToString(),v[3].type.ToString()));
      Console.WriteLine( String.Format("|\\\\\\₪₪₪₪₪| Attack: {0,-3}|₪₪₪₪₪| Attack: {1,-3}|₪₪₪₪₪| Attack: {2,-3}|₪₪₪₪₪| Attack: {3,-3}|₪₪₪₪₪",v[0].attack.ToString(),v[1].attack.ToString(),v[2].attack.ToString(),v[3].attack.ToString() ) );
      Console.WriteLine( String.Format("|\\\\\\₪₪₪₪₪| Defenc: {0,-3}|₪₪₪₪₪| Defenc: {1,-3}|₪₪₪₪₪| Defenc: {2,-3}|₪₪₪₪₪| Defenc: {3,-3}|₪₪₪₪₪",v[0].defence.ToString(),v[1].defence.ToString(),v[2].defence.ToString(),v[3].defence.ToString() ) );
      Console.WriteLine( String.Format("|\\\\\\₪₪₪₪₪| Agilit: {0,-3}|₪₪₪₪₪| Agilit: {1,-3}|₪₪₪₪₪| Agilit: {2,-3}|₪₪₪₪₪| Agilit: {3,-3}|₪₪₪₪₪",v[0].agility.ToString(),v[1].agility.ToString(),v[2].agility.ToString(),v[3].agility.ToString() ) );
      Console.WriteLine( String.Format("|\\\\\\₪₪₪₪₪|   Cost: {0,-3}|₪₪₪₪₪|   Cost: {1,-3}|₪₪₪₪₪|   Cost: {2,-3}|₪₪₪₪₪|   Cost: {3,-3}|₪₪₪₪₪",v[0].Bgold.ToString(),v[1].Bgold.ToString(),v[2].Bgold.ToString(),v[3].Bgold.ToString() ) );
        Console.WriteLine("|\\\\\\₪₪₪₪₪ ============ ₪₪₪₪₪ ============ ₪₪₪₪₪ ============ ₪₪₪₪₪ ============ ₪₪₪₪₪ ");
        Console.WriteLine("|\\\\\\₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪ ");
        Console.WriteLine("|\\\\\\₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪❯❯❯❯❯❯❯❯ CHOOSE WHICH U WANT❮❮❮❮❮❮❮❮₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪ ");
        Console.WriteLine("|\\\\\\₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪ ");
        Console.WriteLine("|\\\\\\₪₪₪₪₪ =====5====== ₪₪₪₪₪ =====6====== ₪₪₪₪₪ =====7====== ₪₪₪₪₪ =====8====== ₪₪₪₪₪ ");
      Console.WriteLine( String.Format("|\\\\\\₪₪₪₪₪|  {0,-10}|₪₪₪₪₪|  {1,-10}|₪₪₪₪₪|  {2,-10}|₪₪₪₪₪|  {3,-10}|₪₪₪₪₪",v[4].name,v[5].name,v[6].name,v[7].name));
      Console.WriteLine( String.Format("|\\\\\\₪₪₪₪₪|   {0,-9}|₪₪₪₪₪|   {1,-9}|₪₪₪₪₪|   {2,-9}|₪₪₪₪₪|   {3,-9}|₪₪₪₪₪",v[4].type.ToString(),v[5].type.ToString(),v[6].type.ToString(),v[7].type.ToString()));
      Console.WriteLine( String.Format("|\\\\\\₪₪₪₪₪| Attack: {0,-3}|₪₪₪₪₪| Attack: {1,-3}|₪₪₪₪₪| Attack: {2,-3}|₪₪₪₪₪| Attack: {3,-3}|₪₪₪₪₪",v[4].attack.ToString(),v[5].attack.ToString(),v[6].attack.ToString(),v[7].attack.ToString() ) );
      Console.WriteLine( String.Format("|\\\\\\₪₪₪₪₪| Defenc: {0,-3}|₪₪₪₪₪| Defenc: {1,-3}|₪₪₪₪₪| Defenc: {2,-3}|₪₪₪₪₪| Defenc: {3,-3}|₪₪₪₪₪",v[4].defence.ToString(),v[5].defence.ToString(),v[6].defence.ToString(),v[7].defence.ToString() ) );
      Console.WriteLine( String.Format("|\\\\\\₪₪₪₪₪| Agilit: {0,-3}|₪₪₪₪₪| Agilit: {1,-3}|₪₪₪₪₪| Agilit: {2,-3}|₪₪₪₪₪| Agilit: {3,-3}|₪₪₪₪₪",v[4].agility.ToString(),v[5].agility.ToString(),v[6].agility.ToString(),v[7].agility.ToString() ) );
      Console.WriteLine( String.Format("|\\\\\\₪₪₪₪₪|   Cost: {0,-3}|₪₪₪₪₪|   Cost: {1,-3}|₪₪₪₪₪|   Cost: {2,-3}|₪₪₪₪₪|   Cost: {3,-3}|₪₪₪₪₪",v[4].Bgold.ToString(),v[5].Bgold.ToString(),v[6].Bgold.ToString(),v[7].Bgold.ToString() ) );
        Console.WriteLine(" \\\\\\₪₪₪₪₪ ============ ₪₪₪₪₪ ============ ₪₪₪₪₪ ============ ₪₪₪₪₪ ============ ₪₪₪₪₪ ");
           Console.WriteLine("  \\\\₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪ ");
          Console.WriteLine("   \\₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪ PRESS 9 TO EXIT SHOP ₪₪₪₪₪₪₪₪₪₪₪₪ \n");
    }

    // S H O P
    public static void shop(Character Char,Base Data,string loc)
    {

      List<Item> v = new List<Item> ();
      if(!shop_check(Data.Itms,v,loc)){
        Console.Write("Shieeet Brawh! Something Wrong \n");
		Console.Read ();
        return;
      }


      Function.shop_table(v);



      while(true)
      {

        Console.Write("Enter Number: ");//buying weapon or armor
		string line = Console.ReadLine();
		int value;
		
		if (int.TryParse (line, out value)) {
			int p = Int32.Parse (line);
				if (p >= 9 || p < 1)
						return;
			Function.shcase (Char, v [p - 1], Data);
		} else {
			Console.WriteLine ("I said number!");
		}


      }

    }


	public static void Save(Base dat,Character you)
		{
			Character c = you;

			string path="Data/savefile.dat";
			StreamWriter writer= new StreamWriter(path);

			if (!File.Exists (path)) {
				Console.WriteLine ("Biatch!");
				Console.Read ();
				return;
			}

			// Char properties
			writer.Write (c.get_name + "," + c.get_strength + "," + c.get_health + "," + c.get_speed + "," + 
							c.get_attack + "," + c.get_defence + "," +c.get_stamina + "," + c.get_xp + "," +
							c.get_max + "," + c.get_lvl + "," + c.get_gold + "," + c.get_player  );

			writer.Write ("\r\n");//newline

			// Location's firstime
			foreach (var l in dat.Loc) {
				if(l==dat.Loc.Last())
					writer.Write (l.firstTime.ToString () );
				else writer.Write (l.firstTime.ToString () + ",");
			}


			// current location
			writer.Write ("\r\n"+Game.Global.a);


			// Char's Gear
			foreach (var x in c.Gear) 
				writer.Write ("\r\n"+ x.name + "," + x.type.ToString () + "," +
					x.attack + "," + x.defence + "," + x.agility + "," +
					x.Bgold + "," + x.Sgold );


			// Char's Inventory
			foreach (var x in c.Inv)
				writer.Write ("\r\n"+ x.name + "," + x.type.ToString () + "," +
					x.attack + "," + x.defence + "," + x.agility + "," +
					x.Bgold + "," + x.Sgold );


			writer.Close ();
		}


				
	public static void Load(Base dat)
	{
			string path = "Data/savefile.dat";
			StreamReader reader= new StreamReader(path);
			if (!File.Exists (path)) {
				Console.WriteLine ("Biatch!");
				Console.Read ();
				return;
			}

			// Input Char
			Character c = new Character (reader);
			dat.Chars.Add (c);// add to base


			// Reads Location's firstime visit
			string loc = reader.ReadLine ();
			var loc_time = loc.Split (new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
			for (int f = 0; f < dat.Loc.Count (); f++){
				dat.Loc [f].firstTime = bool.Parse(loc_time [f]);
			}

			// Current Location
			int cur=Int32.Parse(reader.ReadLine());
			Game.Global.a = cur;

			// Reads Char's Gear
			for( int g = 0; g < c.Gear.Count() ; g++)
			{
				Item X = new Item (reader);
				c.Gear[g]=X;
			}


			// Reads Char's Inventory
			for( int g = 0; g < c.Inv.Count() ; g++)
			{
				Item X = new Item (reader);
				c.Inv[g]=X;
			}




			reader.Close ();

	}


  }//class

}//namespace
