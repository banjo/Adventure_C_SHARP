﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Functions;

namespace AdventureGame
{
    // Gear type
	public enum GearType { Weapon1, Weapon2, Head, Chest, Pants, Boots }

    // Inventory item type
    public enum InvType { NoItem, Sword, Axe, Bow, Shield, Head, Chest, Pants, Boots, Armor, Modifier, QuestItem, Potion, Other }

    // item class
    public class Item
    {
        public string name { get; set; }
        //private string type { get; set; }
        public InvType type { get ; set; }
        public int attack { get; set; }
        public int defence { get; set; }
        public int agility { get; set; }

        public int Bgold { get; set; } // Item Buy Price
        public int Sgold { get; set; } // Item sell Price

        // ITEMS MODIFIERS MAYBE ADDED

        // Constructors
        public Item()
        {
            name = "empty";
            type = InvType.NoItem;
            attack = 0;
            defence = 0;
            agility = 0;
            Bgold = 0;
            Sgold = 0;
        }


        public Item(string name, string type, int attack, int defence, int agility, int Bgold, int Sgold)
        {
            this.name = name;
            this.type = (InvType)Enum.Parse(typeof(InvType), type);
            this.attack = attack;
            this.defence = defence;
            this.agility = agility;
            this.Bgold = Bgold;
            this.Sgold = Sgold;
        }


		public Item( System.IO.StreamReader rd)
		{
			string line = rd.ReadLine ();
			var lineSplit = line.Split (new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

			name = lineSplit [0];
			type = (InvType)Enum.Parse(typeof(InvType),  lineSplit [1]);
			attack = Int32.Parse(lineSplit [2]);
			defence = Int32.Parse(lineSplit [3]);
			agility = Int32.Parse(lineSplit [4]);
			Bgold = Int32.Parse(lineSplit [5]);
			Sgold = Int32.Parse(lineSplit [6]);


		}


    }


    // inventory class
    public class Inventory
    {
        public Item[] Gear;
        public Item[] Inv;
        public static int curIndex;//checks item
        public static Item Empty = new Item();

        // Constr
        public Inventory()
        {
            Gear = new Item[6];
            for (int i=0; i<6; ++i)
                Gear [i] = new Item ();
            Inv = new Item[50];
            for (int i=0; i<50; ++i)
                Inv [i] = new Item ();
        }

        // checks if inventory is empty
        public bool isEmpty()
        {
            for(int i=0 ;i <Game.Global.invSize;++i)
            {
                if ( Inv[i].type == InvType.NoItem )
                {
                    curIndex = i;
                    return true;
                }
            }
            return false;
        }

        // Insert item
        public bool insertItem(Item i)
        {
            if (isEmpty())
            {
                this.Inv[curIndex] = i;
                Console.WriteLine("DONE!");
                return true;
            }
            else Console.WriteLine("MOVE BITCH!");

            return false;
        }

        // Insert item
        public  void insertArmor(Character xx,Item i,List<Item> inv )
        {
			if(xx.get_gold>=i.Bgold && isEmpty())
            {

                for(int k=18;k<38;k++)
                {
					if(inv[k].name==i.name && inv[k].type!=InvType.Armor)
					{
						for(int ind=0; ind<Game.Global.invSize;ind++)
						{
							if (Inv [ind].type == InvType.NoItem) {
								Inv [ind] = inv [k];
								break;
							}
							
						}
					}
                }

                Console.Write("You bought "+i.name+" armor!! \n");


                xx.set_gold=xx.get_gold-i.Bgold;

            }
			else {Console.Write("You dont have enough gold or Your Inventory is Full\n"); Console.ReadLine ();}
        }

        // Delete item
        public void delete(int index)
        {
			Inv[index] = Empty; // swap empty and current item
        }

        // Move Item to Inventory
        public void MoveToInv (Character X,GearType G )
        {
			if (isEmpty ()) {
				Console.Write ("Done! {0} {1} moved to Inventory.", Gear [(int)G].name, Gear [(int)G].type.ToString ());
				Function.del_prop(X, Gear [(int)G]);
				Inv [curIndex] = Gear [(int)G];
				Gear [(int)G] = Empty;

				Console.Read ();
			} else {
				Console.WriteLine ("U cant bitch!");
				Console.ReadLine ();
			}

        }

        // Move Item to Gear
		public void MoveToGear (Character X,int index)
		{       // Slot Empty

			string[] weapons = { "Sword", "Bow", "Axe" };

			if (weapons.Contains (Inv [index].type.ToString ())) {
				if (Gear [0].type == InvType.NoItem) {
					Console.Write ("Done! {0} {1} moved to Gear.", Inv [index].name, Inv [index].type.ToString ());
					Function.prop (X, Inv [index]);

					Gear [0] = Inv [index];
					Inv [index] = Empty;

					Console.Read ();
				} else if (Gear [1].type == InvType.NoItem) {
					Console.Write ("Done! {0} {1} moved to Gear.", Inv [index].name, Inv [index].type.ToString ());
					Function.prop (X, Inv [index]);

					Gear [1] = Inv [index];
					Inv [index] = Empty;

					Console.Read ();
				} else {
					Console.Write ("Your Weapon Slots is Full! Do you want to change itmes? [ Y/N ]: ");
					if (Console.ReadLine () == "Y") {
						Item swap = new Item ();
						Console.Write ("Choose Slot: ");
						int slot = Int32.Parse (Console.ReadLine ());

						Function.del_prop(X, Inv [index]);
						Function.prop (X, Inv [index]);
						swap = Gear [slot - 1];
						Gear [slot - 1] = Inv [index];
						Inv [index] = swap;

						Console.Write ("Changed!");
						Console.Read ();

					} else {
						// Slot Full
						Console.Write ("Bitch Better Have ma money!(FULL)");
						Console.ReadLine ();
					}

				}
	
			} else {//
				for (int x = 0; x < Gear.Count (); x++) {
					if (((GearType)x).ToString () == Inv [index].type.ToString ()) {
						Console.Write ("Done! {0} {1} moved to Gear.", Inv [index].name, Inv [index].type.ToString ());
						Function.prop (X, Inv [index]);
						Gear [x] = Inv [index];
						Inv [index] = Empty;
						Console.Read ();
						return;
					}
				}
			 
            
			}
		}

    }//Inventory


    public class Locations
    {
        //friend void shop(Base*,const string loc);//shop.cpp
        //friend void battle(Character&, Character&);//battle.cpp


        public string loc_name;
        public string shop_tbl;
        public int price;
        public bool firstTime;
        public List<string> func;

        // Constructors
        public Locations(){}
        public Locations(string n,string m,int p, bool b)
        {
            loc_name=n;
            shop_tbl=m;
            price=p;
            firstTime=b;
        }
        ~Locations() { }

        // travel to town
        public void town(Character Char,List<Locations> Loc)
        {
            if(Loc[Game.Global.a].loc_name==this.loc_name){
                Console.WriteLine("You are here u dumbass!!! :D ");
                Console.Read ();
                return ;
            }
            if(Char.get_gold < this.price){
                Console.WriteLine("You don't have enough money to visit this location!!!");
                Console.Write ("Press enter to continue...");
                Console.Read ();
                return;
            }
            else{
                Char.set_gold = Char.get_gold - this.price;
                Game.Global.a = Loc.IndexOf (this);
            }
            return;
        }

        // Choose Town
        public void travel(Character Char,Game.Base Data)
        {
            Console.WriteLine ("Choose Destination");
            int i=0;
            while (i < Data.Loc.Count)
            {
                if( !Data.Loc[i].firstTime) //DAYUUUM SON!!! tu qalaqi agmochenili ar gaqvs travelshi ar dagiwers :>
                {
                    Console.WriteLine ( (i+1) + ". " + String.Format("{0,-20}",Data.Loc[i].loc_name ) + "- cost: " + Data.Loc[i].price);
                    i++;
                }
                else break;
            }
            Console.WriteLine( (i+1) + ". EXIT ");

            var trloc = Console.ReadLine ();
            if (trloc == "" || Int32.Parse(trloc)>i)
                return;

            Data.Loc[Int32.Parse(trloc)-1].town(Char,Data.Loc);

            return;
        }

    }//locations


}//namespace
