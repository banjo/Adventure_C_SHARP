﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AdventureGame
{
    public class Character : Inventory
    {

        protected string name;
        protected int strength;
		protected int health;
		protected int speed;

        protected int attack ;
		protected int defence ;
        protected int stamina ;

        protected int xp;
        protected int max_xp;
        protected int lvl;

        protected int gold;
        protected bool isplayer;

        // CHAR's INFO
        public string[] info = new string[9];
        //List<Quest> quests = new List<Quest>;

        //Constructors
        public Character() : base() /*Inventory*/ { }
		public Character( System.IO.StreamReader rd)
		{
			string line = rd.ReadLine ();
			var lineSplit=line.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
			name = lineSplit [0];
			strength = Int32.Parse(lineSplit [1]);
			health = Int32.Parse(lineSplit [2]);
			speed = Int32.Parse(lineSplit [3]);

			attack = Int32.Parse(lineSplit [4]);
			defence = Int32.Parse(lineSplit [5]);
			stamina = Int32.Parse(lineSplit [6]);

			xp = Int32.Parse(lineSplit [7]);
			max_xp = Int32.Parse(lineSplit [8]);
			lvl = Int32.Parse(lineSplit [9]);

			gold = Int32.Parse(lineSplit [10]);
			isplayer = bool.Parse(lineSplit [11]);
		}
        public Character(string name, int str, int hel, int spe, int att, int def, int sta, int xp, int max, int lvl, int gold, bool is_p)
        {
            this.name = name;
            this.strength = str;
			this.health = hel;
			this.speed = spe;

            this.attack = att;
			this.defence = def;
            this.stamina = sta;

            this.xp = xp;
            this.max_xp = max;
            this.lvl = lvl;

            this.gold = gold;
            this.isplayer = is_p;
        }

        ~Character() { }//Destructor?

        //Character(istream &);


        // Set Functions
        public string set_name { set { name = value; } }
        public int set_health { set { health = value; } }
        public int set_strength { set { strength = value; } }
        public int set_speed { set { speed = value; } }
        public int set_xp { set { xp = value; } }
        public int set_gold { set { gold = value; } }
        public bool set_player { set { isplayer = value; } }
        public int set_attack { set { attack = value; } }
        public int set_defence { set { defence = value; } }
        public int set_stamina { set { stamina = value; } }

        // Get Functions
        public string get_name { get { return this.name; } }
        public int get_health { get { return this.health; } }
        public int get_strength { get { return this.strength; } }
        public int get_speed { get { return this.speed; } }
        public int get_xp { get { return this.xp; } }
        public int get_max { get { return this.max_xp; } }
        public int get_lvl { get { return this.lvl; } }
        public int get_gold { get { return this.gold; } }
        public bool get_player { get { return this.isplayer; } }
        public int get_attack { get { return this.attack; } }
        public int get_defence { get { return this.defence; } }
        public int get_stamina { get { return this.stamina; } }




        //Random
        Random rnd = new Random();

		public void sub_speed() { this.set_speed =get_speed-rnd.Next(0, 20); }// - speed
		public void inc_speed() {this.set_speed =get_speed+rnd.Next(0, 20); }// + speed

        //Level Up
        public void level_up()
        {
            lvl++;
			set_health = Functions.Function.MAX_HEALTH;
            //upgrade health and speed
            set_health = rnd.Next(health, health * lvl);
            set_health = rnd.Next(strength, strength * lvl);
            set_speed = rnd.Next(1, lvl / 2 * get_speed);
        }


        //Add XP
        public void addxp(int xx)
        {
            xp += xx;
            if (xp >= max_xp)
            {
                xp = 0;
                if (lvl < 2)
                  max_xp += rnd.Next (max_xp, max_xp + max_xp / 2);
                else
                  max_xp += rnd.Next(max_xp, max_xp * lvl - xp + 30);//i have no idea what i'm doing
				
                level_up();
            }
        }

        //Slap det bitch
        public int hit()
        {
          int hh;
          if(attack<strength)
            hh = rnd.Next(attack, strength + 1);
          else
            hh = rnd.Next(strength, attack + 1);
          Console.Write("Damaged {0} hp . ", hh);

          return hh;

        }

        // Update
        public void update(string[] arr)
        {

            arr[0] = "++++++++++++++++++++++++";
            arr[1] = "| Name:      " + String.Format("{0,-9}  ", get_name) ;
            arr[2] = "| Strength:  " + String.Format("{0,3} + {1,-4} ", get_strength, get_attack) ;
			arr[3] = "| Health:    " + String.Format("{0,3} + {1,-4} ", get_health, get_defence) ;
			arr[4] = "| Speed:     " + String.Format("{0,3} + {1,-4} ", get_speed, get_attack) ;
            arr[5] = "| Exp:      (" + String.Format("{0,4}/{1,-4}", xp, max_xp) + ") ";
            arr[6] = "| Level:     " + String.Format("{0,-9}  ", lvl) ;
            arr[7] = "| Gold:      " + String.Format("{0,-9}  ", gold);
            arr[8] = "++++++++++++++++++++++++";

            return;
        }

        // Print Info
        public void print()
        {
            for (int id = 0; id < info.Length; id++)
                Console.WriteLine(info[id]);
        }


    }//CHARACTER 

    // SUB CLASSES ( RACES )
    class Orc : Character
    {
        public Orc()
        {
            name = "orc";
            strength = 25 + attack;
            speed = 150 + stamina;
            health = 40 + defence;
            xp = 0;
            max_xp = 100;
            gold = 100;
            lvl = 1;
            isplayer = false;
            update(info);
        }

        ~Orc() { }
    };

    class Human : Character
    {

        public Human()
        {
            name = "human";
            strength = 15 + attack;
            speed = 130 + stamina;
            health = 50 + defence;
            xp = 0;
            max_xp = 100;
            gold = 10000;
            lvl = 1;
            isplayer = false;
            update(info);
        }
        ~Human() { }
    };

    class Elf : Character
    {

        public Elf()
        {
            name = "elf";
            strength = 20 + attack;
            speed = 130 + stamina;
            health = 100 + defence;
            xp = 0;
            max_xp = 100;
            gold = 10000;
			lvl = 1;
            isplayer = false;
            update(info);
        }
        ~Elf() { }
    };

    // C ++ stuff



    //Character & operator = (Character* ); ===============>>>> nope nope


    //friend ostream& operator (ostream& ,List<Character*> );
    //void printfile(ostream& );
    //friend void battle(Character*, Character*);//battle.cpp
    // friend void shop(Character*, Base*,const string& );//shop.cpp
    //friend void prop(Character& , Items& );
    //friend void del_prop(Character& , Items& );
}//namespace
